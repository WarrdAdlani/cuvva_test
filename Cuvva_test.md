Cuvva test

Hi :)

I want to first say that the test was a pleasure, and I appreciated the challenge of learning more about insurance in order to better understand the data given back.

I took roughly 5 hours to do this due to a hectic week, so I chose to use the time to give an overview of the architecture that I think is rather suitable for the task. It's VIPER and Clean Code approaches which is why there are frameworks/projects in the app (Networking, Domain). This was done to maintain separation of concerns, and to keep the project organised.

I was tempted to add in reactive, as I could really see the benefit in such a project for FRP, but I left it out for brevity sake.

I intended to use Core Data for persistence, which is not implemented, again for brevity, but essentially the classes would conform to Codable, and be stored upon retrieval on a lazy basis to be more efficient. The addtional layer missing is the Data layer for data saving/retrieving.

Challenges I faced where of a more specific nature i.e. the type of "events", and the fact that the JSON is mixed in different events. I'm not sure why that is, but it did require me to code parts that are usually assumed for the backend. Again this is probably because I'm fresh to how insurance policies work and mid way through, it hit me.

I took the liberty of making decisions for text sizes and colours as there's no sizes or hex/rgb for colours, so I hope it looks good.

Thank you for taking the time to look over the test and view this read me.