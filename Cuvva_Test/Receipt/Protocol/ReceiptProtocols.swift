//
//  ReceiptProtocols.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Domain

protocol ReceiptRouterProtocol: class {
    func createModule(with transaction: Policy?) -> UIViewController
}

protocol ReceiptViewProtocol: class {
    var presenter: ReceiptPresenterProtocool! { get set }
    
    func showReceipt()
}

protocol ReceiptPresenterProtocool: class {
    var router: ReceiptRouterProtocol? { get set }
    var view: ReceiptViewProtocol? { get set }
    var transaction: Policy! { get set }
    var numberOfSections: Int { get }
    
    func viewDidLoad()
    func showReceipt()
    func numberOfRows(for section: Int) -> Int 
    func setupCell(_ cell: TransactionCellProtocol, with indexPath: IndexPath)
    func titleForSection(_ section: Int) -> String
}
