//
//  TransactionCell.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 04/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import Domain

enum TransactionField: Int {
    case premium
    case tax
    case adminFee
    case totalPaid
    case grandTotal
}
protocol TransactionCellProtocol {
    func setupCell(with transaction: Policy, index: Int)
}

class TransactionCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}

extension TransactionCell: TransactionCellProtocol {
    func setupCell(with transaction: Policy, index: Int) {
        let section: TransactionField = TransactionField(rawValue: index)!
        var title: String?
        var value: String?
        guard let pricing = transaction.payload.pricing else {
            fatalError()
        }
        var emphasis = false
        
        switch section {
        case .premium:
            title = "Insurance premium"
            value = "£\(Float(pricing.totalPremium)/100.0)"
        case .tax:
            title = "Insurance premium tax"
            value = "£\(Float(pricing.ipt)/10)"
        case .adminFee:
            title = "Admin fee"
            value = "£\(Float(pricing.extraFees)/100.0)"
        case .totalPaid:
            title = "Total paid"
            value = "£\(Float(pricing.totalPayable)/100.0)"
            emphasis = true
        case .grandTotal:
            title = "Grand total"
            value = "£\(Float(pricing.totalPayable)/100.0)"
            emphasis = true
        }
        
        if emphasis {
            titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
            valueLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        }
        
        titleLabel.text = title
        valueLabel.text = value        
    }
    
}
