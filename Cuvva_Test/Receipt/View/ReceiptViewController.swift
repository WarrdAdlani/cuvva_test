//
//  ReceiptViewController.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class ReceiptViewController: UIViewController, ReceiptViewProtocol {
    var presenter: ReceiptPresenterProtocool!
    @IBOutlet weak var voidToastStackView: UIStackView!
    @IBOutlet weak var toastViewHeightConstraint: NSLayoutConstraint!
    
    let voidToastHeight: CGFloat = 24.0
    var showVoidToast = false {
        didSet {
            if showVoidToast {
                toastViewHeightConstraint.constant = voidToastHeight
                voidToastStackView.isHidden = false
            } else {
                toastViewHeightConstraint.constant = 0.0
                voidToastStackView.isHidden = true
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Receipt"
        
        navigationController?.navigationBar.tintColor = Constants.Colours.lightBlue
        navigationController?.navigationBar.barTintColor = Constants.Colours.lightBlue
        
        presenter.viewDidLoad()
        
        showVoidToast = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Constants.Colours.darkBlue]
    }
    
    func showReceipt() {
        // logic here to show toast for void policies
    }
}

extension ReceiptViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell") as? TransactionCellProtocol else {
            fatalError()
        }
        
        presenter.setupCell(cell, with: indexPath)
        
        return cell as! UITableViewCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows(for: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 128.0
    }
}

extension ReceiptViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView = CustomTableHeaderView()
            let view = Bundle.main.loadNibNamed("CustomTableHeaderView", owner: headerView, options: nil)?.first as! CustomTableHeaderView
            
            view.titleLabel.text = presenter.titleForSection(section)
            return view
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return Constants.TableConstants.standardHeaderHeight
        }
        return 0.0
    }
}

