//
//  ReceiptRouter.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

import Domain

class ReceiptRouter: Router, ReceiptRouterProtocol {
    weak var view: UIViewController?
    
    func createModule(with policy: Policy?) -> UIViewController {
        guard let policy = policy else {
            fatalError("policy cannot be nil")
        }
        let view = viewController(name: "ReceiptViewController") as! ReceiptViewProtocol
        let presenter = ReceiptPresenter(transaction: policy)
        view.presenter = presenter
        self.view = view as? UIViewController
        return view as! UIViewController
    }
}
