//
//  ReceiptPresenter.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Domain

class ReceiptPresenter: ReceiptPresenterProtocool {
    var router: ReceiptRouterProtocol?
    var view: ReceiptViewProtocol?
    var transaction: Policy!
    var numberOfSections: Int = 2
    
    init(transaction: Policy) {
        self.transaction = transaction
    }
    
    func viewDidLoad() {}
    
    func showReceipt() {}
    
    func setupCell(_ cell: TransactionCellProtocol, with indexPath: IndexPath) {
        guard let transaction = transaction else {
            return
        }
        if indexPath.section == 0 {
            cell.setupCell(with: transaction, index: indexPath.row)
        } else {
            cell.setupCell(with: transaction, index: 4)
        }
    }
    
    func titleForSection(_ section: Int) -> String {
        let date = Date.dateFromString(transaction!.timestamp, format: .dayMonthYearTimeLong)?.dateAsString()
        guard var components = date?.components(separatedBy: " ") else {
            fatalError()
        }
        let day: String = components[0]
        var dayNumber: String = components[1]
        let month: String = components[2]
        let year: String = components[3]
        
        switch (dayNumber) {
        case "1" , "21" , "31":
            dayNumber.append("st")
        case "2" , "22":
            dayNumber.append("nd")
        case "3" ,"23":
            dayNumber.append("rd")
        default:
            dayNumber.append("th")
        }
        return "\(day) \(dayNumber) \(month) \(year)"
    }
    
    func numberOfRows(for section: Int) -> Int {
        if section == 0 {
            return 4
        }
        return 1
    }
}
