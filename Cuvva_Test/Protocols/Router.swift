//
//  Router.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import UIKit

protocol Router {
    func storyboard(name: String, bundle: Bundle) -> UIStoryboard
    func viewController(name: String) -> UIViewController
}

extension Router {
    func storyboard(name: String = "Main", bundle: Bundle = Bundle.main) -> UIStoryboard {
        return UIStoryboard.init(name: name, bundle: bundle)
    }
    
    func viewController(name: String) -> UIViewController {
        let view = storyboard().instantiateViewController(withIdentifier: name)
        return view
    }
}
