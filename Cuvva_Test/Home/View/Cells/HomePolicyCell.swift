//
//  HomePolicyCell.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import Domain
import UICircularProgressRing

protocol HomePolicyCellProtocol {
    func setupCell(with model: HomePolicyViewModel?)
}

class HomePolicyViewModel {
    var policy: Policy
    var insureBlock: ((Policy)->())?
    var extendBlock: ((Policy)->())?
    var isCurrent: Bool {
        guard let startDate = Date.dateFromString(policy.payload.startDate!, format: .dayMonthYearTimeLong) else {
            return false
        }
        guard let endDate = Date.dateFromString(policy.payload.endDate!, format: .dayMonthYearTimeLong) else {
            return false
        }
        
        return endDate > startDate
    }
    
    func insure() {
        insureBlock?(policy)
    }
    
    func extend() {
        extendBlock?(policy)
    }
    
    init(policy: Policy) {
        self.policy = policy
    }
}

class HomePolicyCell: UITableViewCell {
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var vehicleMakeLabel: UILabel!
    @IBOutlet weak var policiesNumberLabel: UILabel!
    @IBOutlet weak var plateLabel: UILabel!
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var vehicleIconImageView: UIImageView!
    @IBOutlet weak var insureButton: UIButton!
    @IBOutlet weak var remainingLabel: UILabel!
    @IBOutlet weak var circularProgressBar: UICircularProgressRing!
    
    fileprivate var viewModel: HomePolicyViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        cellBackgroundView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
        cellBackgroundView.layer.borderWidth = 1
        cellBackgroundView.layer.cornerRadius = Constants.LayerConstants.cornerRadius
        
        insureButton.layer.cornerRadius = insureButton.frame.height/2
    }
    
    @IBAction func insure(_ sender: Any) {
        viewModel.insure()
    }
}

extension HomePolicyCell: HomePolicyCellProtocol {
    func setupCell(with model: HomePolicyViewModel?) {
        viewModel = model
        
        if let model = model {
            guard let make: Make = model.policy.payload.vehicle?.make else {
                return
            }
            
            vehicleMakeLabel.text =  make.rawValue
            policiesNumberLabel.text = "\(model.policy.transactions?.count ?? 0)"
            plateLabel.text = model.policy.payload.vehicle?.prettyVrm
            makeLabel.text = model.policy.payload.vehicle?.model
            
            var image: String? = nil
            switch make {
            case .ford:
                break
            case .mercedesBenz:
                image = "mercedes-benz"
            case .mini:
                image = "mini"
            case .nissan:
                break
            case .volkswagen:
                image = "volkswagen"
            }
            
            // TODO: Button for policies not expired
            // Extend Colour 33CC99
            
            // Insure
            if viewModel.isCurrent {
                insureButton.backgroundColor = Constants.Colours.whiteBackground
                insureButton.setTitleColor(Constants.Colours.lightBlue, for: .normal)
                insureButton.setTitle("Insure", for: .normal)
            } else {
                // Extend
                insureButton.backgroundColor = Constants.Colours.lightGreen
                insureButton.setTitleColor(Constants.Colours.white, for: .normal)
                insureButton.setTitle("Extend", for: .normal)
            }
            
            if let image = image {
                vehicleIconImageView.image = UIImage(named: image)
                
                vehicleIconImageView.image = vehicleIconImageView.image?.withRenderingMode(.alwaysTemplate)
                vehicleIconImageView.tintColor = Constants.Colours.darkBlue
            }
        }
    }
}
