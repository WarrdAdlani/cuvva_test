//
//  HomeViewController.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, HomeViewProtocol {

    @IBOutlet var buttonContainerViews: [UIView]!
    @IBOutlet weak var tableView: UITableView!

    
    var refreshControl: UIRefreshControl!
    var presenter: HomePresenterProtocol!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .allEvents)
        tableView.addSubview(refreshControl)
        tableView.sectionFooterHeight = 0.0
        tableView.tableFooterView = UIView()
        
        for view in buttonContainerViews {
            view.layer.cornerRadius = Constants.LayerConstants.cornerRadius
        }
        
        presenter.viewDidLoad()
    }
    
    @IBAction func travel(_ sender: Any) {
        // TODO: Placeholders for travel actions
    }
    
    @IBAction func motor(_ sender: Any) {
        // TODO: Placeholders motor actions
    }
    
    @objc func refresh() {
        presenter.refresh()
    }
    
    func showPolicies() {
        tableView.reloadData()
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }
    
    func showError(_ error: Error) {
        showError(with: error)
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows(for: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomePolicyCell") as? HomePolicyCellProtocol else {
            fatalError()
        }
        
        presenter.setupCell(cell, with: indexPath)
        
        return cell as! UITableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 128.0
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = CustomTableHeaderView()
        let view = Bundle.main.loadNibNamed("CustomTableHeaderView", owner: headerView, options: nil)?.first as! CustomTableHeaderView
        view.titleLabel.text = presenter.titleForSection(section)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            guard let activePolicies = presenter.activePolicies else {
                return 0
            }
            return activePolicies.count > 0 ? Constants.TableConstants.standardHeaderHeight : 0
        }
        return Constants.TableConstants.standardHeaderHeight
    }
}
