//
//  HomePagePresenter.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Domain

enum PolicyState: Int {
    case active = 0
    case other
}

class HomePresenter: HomePresenterProtocol {
    weak var view: HomeViewProtocol?
    var interactor: HomeInputInteractorProtocol?
    var inactivePolicies: Policies?
    var activePolicies: Policies?
    
    weak var router: HomeRouterProtocol?
    var numberOfSections: Int {
        return 2
    }
    
    init(router: HomeRouterProtocol) {
        self.router = router
    }
    
    func viewDidLoad() {
        interactor?.getPolicies()
    }
    
    func setupCell(_ cell: HomePolicyCellProtocol, with indexPath: IndexPath) {
        guard let policy = inactivePolicies?[indexPath.row] else {
            return
        }
        
        let viewModel = HomePolicyViewModel(policy: policy)
        viewModel.insureBlock = { [weak self] policy in
            self?.showPolicy(with: policy)
        }
        cell.setupCell(with: viewModel)
    }
    
    func numberOfRows(for section: Int) -> Int {
        switch section {
        case 0:
            return activePolicies?.count ?? 0
        case 1:
            return inactivePolicies?.count ?? 0
        default:
            return 0
        }
    }
    
    func refresh() {
        interactor?.getPolicies()
    }
    
    func titleForSection(_ section: Int) -> String {
        if let state: PolicyState = PolicyState(rawValue: section) {
            switch state {
                case .active:
                    return "Active policies"
                case .other:
                    return "Vehicles"
            }
        }
        
        return ""
    }
    
    func showPolicy(with policy: Policy?) {
        guard let context = view as? UIViewController else {
            fatalError("Could not case view as UIViewController")
        }
        self.router?.showPolicy(context: context, with: policy)
    }
}

extension HomePresenter: HomeOutputInteractorProtocol {
    func didGetPolicies(_ policies: Policies) {
        var active = Policies()
        var inactive = Policies()
        for policy in policies {
            if let startDate = Date.dateFromString(policy.payload.startDate!, format: .dayMonthYearTimeLong),
                let endDate = Date.dateFromString(policy.payload.endDate!, format: .dayMonthYearTimeLong)  {
                
                if  Date().isBetween(startDate, and: endDate) {
                    active.append(policy)
                } else {
                    inactive.append(policy)
                }
            }
        }
        
        self.inactivePolicies = inactive
        self.activePolicies = active
        
        view?.showPolicies()
    }
    
    func didGetError(_ error: Error) {
        view?.showError(error)
    }
}
