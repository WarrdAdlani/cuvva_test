//
//  PoliciesWorker.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Domain
import Networking

struct PoliciesWorker {
    fileprivate var webService: WebService
    
    init(webService: WebService) {
        self.webService = webService
    }
    func getPolicies(completion: @escaping NetworkResponseCompletion) {
        webService.getPolicies { (policies, error) in
            guard let policies = policies as? Policies else {
                fatalError()
            }
            
            // Add transactions to policies
            let prettyPolicies = policies.filter {
                return $0.type == PolicyStateType.policyCreated
            }.map { policy -> Policy in
                // Find transactiosn related to a policy and add it to the policy
                policy.transactions = policies.filter {
                    return $0.type == PolicyStateType.policyFinancialTransaction && $0.payload.policyID == policy.payload.policyID
                }
                return policy
            }
            
            completion(prettyPolicies, error)
        }
    }
}
