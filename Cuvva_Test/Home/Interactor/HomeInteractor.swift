//
//  HomePageInteractor.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Networking
import Domain

class HomeInteractor: HomeInputInteractorProtocol {
    weak var presenter: HomeOutputInteractorProtocol?
    
    fileprivate var webService: WebService?
    fileprivate var worker: PoliciesWorker?
    
    init(webService: WebService, worker: PoliciesWorker) {
        self.webService = webService
        self.worker = worker
    }
    
    func getPolicies() {
        worker?.getPolicies(completion: { [weak self] (result, error) in
            if let result = result as? Policies {
                self?.presenter?.didGetPolicies(result)
            } else if let error = error {
                self?.presenter?.didGetError(error)
            }
        })
    }
}
