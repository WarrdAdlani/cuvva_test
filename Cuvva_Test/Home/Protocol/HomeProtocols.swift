//
//  HomePageProtocols.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import UIKit
import Domain

protocol HomeRouterProtocol: class {
    func createModule() -> UIViewController
    func showPolicy(context: UIViewController, with policy: Policy?)
}

protocol HomeViewProtocol: class {
    var presenter: HomePresenterProtocol! { get set }
    
    func showPolicies()
    func showError(_ error: Error)
}

protocol HomePresenterProtocol: class {
    var view: HomeViewProtocol? { get set }
    var interactor: HomeInputInteractorProtocol? { get set }
    var inactivePolicies: Policies? { get }
    var activePolicies: Policies? { get }
    var numberOfSections: Int { get }
    var router: HomeRouterProtocol? { get set }
    
    func viewDidLoad()
    func refresh()
    func setupCell(_ cell: HomePolicyCellProtocol, with indexPath: IndexPath)
    func titleForSection(_ section: Int) -> String
    func showPolicy(with policy: Policy?)
    func numberOfRows(for section: Int) -> Int
}

protocol HomeInputInteractorProtocol: class {
    var presenter: HomeOutputInteractorProtocol? { get set }
    
    func getPolicies()
}

protocol HomeOutputInteractorProtocol: class {
    func didGetPolicies(_ policies: Policies)
    func didGetError(_ error: Error)
}
