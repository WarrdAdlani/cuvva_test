//
//  HomePageRouter.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import UIKit
import Networking
import Domain

class HomeRouter: Router, HomeRouterProtocol {
    var policyRouter: PolicyRouterProtocol?
    
    func createModule() -> UIViewController {
        // Initializations
        let view = viewController(name: "HomeViewController") as! HomeViewProtocol
        let webService = WebService()
        let worker = PoliciesWorker(webService: webService)
        let interactor = HomeInteractor(webService: webService, worker: worker)
        let presenter: HomePresenterProtocol & HomeOutputInteractorProtocol = HomePresenter(router: self)
        
        // Assignments
        presenter.interactor = interactor
        interactor.presenter = presenter
        presenter.view = view
        view.presenter = presenter
        
        return view as! UIViewController
    }
    
    func showPolicy(context: UIViewController, with policy: Policy?) {
        policyRouter = PolicyRouter()
        guard let view = policyRouter?.createModule(with: policy) else {
            return
        }
        let navigationController = UINavigationController(rootViewController: view)
        context.present(navigationController, animated: true, completion: nil)
    }
}
