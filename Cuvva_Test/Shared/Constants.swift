//
//  Constants.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 04/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import UIKit

public struct Constants {
    
    public struct LayerConstants {
        static let cornerRadius: CGFloat = 8.0
    }
    
    public struct Colours {
        static let white = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        static let whiteBackground = #colorLiteral(red: 0.9529411765, green: 0.9529411765, blue: 1, alpha: 1)
        static let lightBlue = #colorLiteral(red: 0.4352941176, green: 0.4666666667, blue: 1, alpha: 1)
        static let darkBlue = #colorLiteral(red: 0.265753448, green: 0.2799136341, blue: 0.5450479984, alpha: 1)
        static let lightGreen = #colorLiteral(red: 0.2189696729, green: 0.825812161, blue: 0.6643946767, alpha: 1)
    }
    
    public struct TableConstants {
        static let standardHeaderHeight: CGFloat = 56.0
    }
    
    public enum DateFormats: String {
        case dayMonthYearTimeLong = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        case dayMonthYearLong = "E, d MMM yyyy"
        case dayMonthYearShort = "dd/MM/yyyy"
        case dayMonthLong = "d MMM"
        case dayMonthShort = "dd/MM"
    }
}
