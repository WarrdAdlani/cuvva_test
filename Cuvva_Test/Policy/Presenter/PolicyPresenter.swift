//
//  PolicyPresenter.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Domain

class PolicyPresenter: PolicyPresenterProtocool {
    weak var router: PolicyRouterProtocol?
    weak var view: PolicyViewProtocol?
    var policy: Policy?
    var numberOfRows: Int {
        guard let transactionCount = policy?.transactions?.count else {
            return 0
        }
        return transactionCount
    }
    var numberOfSections: Int {
        return 2
    }
    var model: String {
        guard let model = policy?.payload.vehicle?.model else {
            fatalError()
        }
        return model
    }
    var licenseNumber: String {
        guard let license = policy?.payload.vehicle?.prettyVrm else {
            fatalError()
        }
        return license
    }
    var numberOfPolicies: String {
        guard let numberOfPolicies = policy?.transactions?.count else {
            return ""
        }
        return "\(numberOfPolicies)"
    }
    var vehicleImageName: String {
        guard let make = policy?.payload.vehicle?.make else {
            fatalError()
        }
        var image = String()
        switch make {
        case .ford:
            break
        case .mercedesBenz:
            image = "mercedes-benz"
        case .mini:
            image = "mini"
        case .nissan:
            break
        case .volkswagen:
            image = "volkswagen"
        }
        
        return image
    }
    
    var vehicleMakeAndModel: String {
        guard let make = policy?.payload.vehicle?.make else {
            fatalError()
        }
        guard let model = policy?.payload.vehicle?.model else {
            fatalError()
        }
        
        return "\(make) \(model)"
    }
    
    init(router: PolicyRouter, view: PolicyViewProtocol, policy: Policy?) {
        self.router = router
        self.view = view
        self.policy = policy
    }
    
    func viewDidLoad() {
        view?.showPolicy()
    }
    
    func showReceipt() {
        
    }
    
    func setupCell(_ cell: PolicyCellProtocol, with indexPath: IndexPath) {
        guard let policy = policy?.transactions?[indexPath.row] else {
            return
        }
        cell.setupCell(with: policy)
    }
    
    func didSelectTransaction(at indexPath: IndexPath) {
        if indexPath.section == 1 {
            guard let transaction = policy?.transactions?[indexPath.row] else {
                return
            }
            
            guard let view = view as? ViewControllerType else {
                fatalError("Could not cast view as ViewControlelrType")
            }
            router?.showReceipt(with: transaction, in: view)
        }
    }
    
    func titleForSection(_ section: Int) -> String {
        if let state: PolicyState = PolicyState(rawValue: section) {
            switch state {
            case .active:
                return "Active driving policy"
            case .other:
                return "Previous driving policies"
            }
        }
        
        return ""
    }
    
    func close() {
        router?.close()
    }
}
