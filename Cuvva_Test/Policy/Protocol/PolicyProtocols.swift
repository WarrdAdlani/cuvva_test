//
//  PolicyProtocols.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import Domain

protocol PolicyRouterProtocol: class {
    func createModule(with policy: Policy?) -> UIViewController
    func showReceipt(with policy: Policy, in context: ViewControllerType)
    func close()
}

protocol PolicyViewProtocol: class {
    var presenter: PolicyPresenterProtocool! { get set }
    
    func showPolicy()
}

protocol PolicyPresenterProtocool: class {
    var router: PolicyRouterProtocol? { get set }
    var view: PolicyViewProtocol? { get set }
    var policy: Policy? { get set }
    var numberOfRows: Int { get }
    var numberOfSections: Int { get }
    var licenseNumber: String { get }
    var model: String { get }
    var numberOfPolicies: String { get }
    var vehicleImageName: String { get }
    var vehicleMakeAndModel: String { get }
    
    func viewDidLoad()
    func showReceipt()
    func setupCell(_ cell: PolicyCellProtocol, with indexPath: IndexPath)
    func titleForSection(_ section: Int) -> String
    func didSelectTransaction(at indexPath: IndexPath)
    func close()
}
