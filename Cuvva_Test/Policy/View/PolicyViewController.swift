//
//  PolicyViewController.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class PolicyViewController: UIViewController, ViewControllerType, PolicyViewProtocol {
    
    @IBOutlet weak var curverView: UIView!
    @IBOutlet weak var extendCoverButton: UIButton!
    @IBOutlet weak var vehicleIconImageView: UIImageView!
    @IBOutlet weak var vehicleIconView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var licensePlateLabel: UILabel!
    @IBOutlet weak var policiesNumberLabel: UILabel!
    @IBOutlet weak var vehicleModel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var presenter: PolicyPresenterProtocool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        curverView.addBottomRoundedEdge(desiredCurve: 2.0)
        curverView.layer.backgroundColor = Constants.Colours.lightBlue.cgColor
        curverView.layer.masksToBounds = true
        extendCoverButton.layer.cornerRadius = 8
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.view.backgroundColor = .clear
        navigationItem.titleView?.tintColor = .white
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        let closeButton = UIButton(type: .custom)
        closeButton.setTitle("Ⅹ", for: .normal)
        closeButton.addTarget(self, action: #selector(close(sender:)), for: .touchUpInside)
        let closeButtonItem = UIBarButtonItem(customView: closeButton)
        navigationItem.setLeftBarButton(closeButtonItem, animated: false)
        
        title = "Vehicle profile"
        
        vehicleIconView.clipsToBounds = true
        presenter.viewDidLoad()
        
        tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
    }
    
    override func viewDidLayoutSubviews() {
        vehicleIconView.layer.cornerRadius = vehicleIconView.frame.height/2
    }
    
    func showPolicy() {
        licensePlateLabel.text = presenter.licenseNumber
        policiesNumberLabel.text = presenter.numberOfPolicies
        vehicleIconImageView.image = UIImage(named: presenter.vehicleImageName)
        vehicleModel.text = presenter.vehicleMakeAndModel.capitalized
        
        vehicleIconImageView.image = vehicleIconImageView.image?.withRenderingMode(.alwaysTemplate)
        vehicleIconImageView.tintColor = Constants.Colours.darkBlue
    }
    
    @objc func close(sender: Any) {
        presenter?.close()
    }
}

extension PolicyViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PolicyCellProtocol?
        
        if indexPath.section == 0 {
            guard let activePolicyCell = tableView.dequeueReusableCell(withIdentifier: "ActivePolicyCell") as? PolicyCellProtocol else {
                fatalError()
            }
            cell = activePolicyCell
        } else {
            guard let previousPolicyCell = tableView.dequeueReusableCell(withIdentifier: "PreviousPolicyCell") as? PolicyCellProtocol else {
                fatalError()
            }
            cell = previousPolicyCell
        }

        presenter.setupCell(cell!, with: indexPath)
        
        return cell as! UITableViewCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return presenter.numberOfRows
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 128.0
    }
}

extension PolicyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = CustomTableHeaderView()
        let view = Bundle.main.loadNibNamed("CustomTableHeaderView", owner: headerView, options: nil)?.first as! CustomTableHeaderView
        view.titleLabel.text = presenter.titleForSection(section)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.TableConstants.standardHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectTransaction(at: indexPath)
    }
}

