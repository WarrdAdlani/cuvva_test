//
//  ActivePolicyCell.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 04/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import Domain
import UICircularProgressRing

class ActivePolicyCell: UITableViewCell {
    @IBOutlet weak var contentContainerView: UIView!
    @IBOutlet weak var policyStackView: UIStackView!
    @IBOutlet weak var timeRemainingProgress: UICircularProgressRing!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        for view in policyStackView.subviews {
            view.layer.cornerRadius = Constants.LayerConstants.cornerRadius
        }
        contentContainerView.layer.cornerRadius = Constants.LayerConstants.cornerRadius
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension ActivePolicyCell: PolicyCellProtocol {
    func setupCell(with policy: Policy) {
        
    }
}
