//
//  PreviousPolicyCell.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 04/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import Domain

protocol PolicyCellProtocol {
    func setupCell(with policy: Policy)
}

class PreviousPolicyCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}

extension PreviousPolicyCell: PolicyCellProtocol {
    func setupCell(with policy: Policy) {
        let date = Date.dateFromString(policy.timestamp, format: .dayMonthYearTimeLong)?.dateAsString()
        guard var components = date?.components(separatedBy: " ") else {
            fatalError()
        }
        let day: String = components[0]
        var dayNumber: String = components[1]
        let month: String = components[2]
        let year: String = components[3]
        
        switch (dayNumber) {
        case "1" , "21" , "31":
            dayNumber.append("st")
        case "2" , "22":
            dayNumber.append("nd")
        case "3" ,"23":
            dayNumber.append("rd")
        default:
            dayNumber.append("th")
        }
        dateLabel.text = "\(day) \(dayNumber) \(month) \(year)"
    }
}
