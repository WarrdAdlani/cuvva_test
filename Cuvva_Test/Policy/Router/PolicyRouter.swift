//
//  PolicyRouter.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import Domain

class PolicyRouter: Router, PolicyRouterProtocol {
    
    weak var view: UIViewController?
    
    func createModule(with policy: Policy?) -> UIViewController {
        let view = viewController(name: "PolicyViewController") as! PolicyViewProtocol
        let presenter = PolicyPresenter(router: self, view: view, policy: policy)
        view.presenter = presenter
        self.view = view as? UIViewController
        return view as! UIViewController
    }
    
    func showReceipt(with policy: Policy, in context: ViewControllerType) {
        let view = ReceiptRouter().createModule(with: policy)
        guard let context = context as? UIViewController else {
            fatalError("Failed to cast context as UIViewController")
        }
        context.navigationController?.pushViewController(view, animated: true)
    }
    
    func close() {
        guard let view = view else {
            return
        }
        if let navigationController = view.navigationController {
            navigationController.dismiss(animated: true, completion: nil)
        } else {
            view.dismiss(animated: true, completion: nil)
        }
    }
}
