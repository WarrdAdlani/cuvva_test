//
//  Policy.swift
//  Domain
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

// MARK: - Policy
public class Policy: Codable {
    public let type: PolicyStateType
    public let timestamp, uniqueKey: String
    public let payload: Payload
    public var transactions: [Policy]?
    
    enum CodingKeys: String, CodingKey {
        case type, timestamp
        case uniqueKey = "unique_key"
        case payload
    }
}
