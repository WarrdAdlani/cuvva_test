//
//  Pricing.swift
//  Domain
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

// MARK: - Pricing
public struct Pricing: Codable {
    public let underwriterPremium, commission, totalPremium, ipt: Int
    public let iptRate, extraFees, vat, deductions: Int
    public let totalPayable: Int
    
    enum CodingKeys: String, CodingKey {
        case underwriterPremium = "underwriter_premium"
        case commission
        case totalPremium = "total_premium"
        case ipt
        case iptRate = "ipt_rate"
        case extraFees = "extra_fees"
        case vat, deductions
        case totalPayable = "total_payable"
    }
}
