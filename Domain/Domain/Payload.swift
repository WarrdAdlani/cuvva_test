//
//  Payload.swift
//  Domain
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

// MARK: - Payload
public struct Payload: Codable {
    public let userID: UserID?
    public let policyID: String
    public let originalPolicyID, referenceCode, startDate, endDate: String?
    public let incidentPhone: String?
    public let vehicle: Vehicle?
    public let documents: Documents?
    public let pricing: Pricing?
    public let type: String?
    public let newEndDate: String?
    
    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case policyID = "policy_id"
        case originalPolicyID = "original_policy_id"
        case referenceCode = "reference_code"
        case startDate = "start_date"
        case endDate = "end_date"
        case incidentPhone = "incident_phone"
        case vehicle, documents, pricing, type
        case newEndDate = "new_end_date"
    }
}
