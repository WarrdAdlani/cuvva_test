//
//  Vehicle.swift
//  Domain
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

// MARK: - Vehicle
public struct Vehicle: Codable {
    public let license, prettyLicense: String
    public let make: Make
    public let model: String
    public let variant: String?
    public let color: Color
    
    enum CodingKeys: String, CodingKey {
        case license = "vrm"
        case prettyLicense = "prettyVrm"
        case make
        case model
        case variant
        case color
    }
}
