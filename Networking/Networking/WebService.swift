//
//  NetworkManager.swift
//  Employees
//
//  Created by Warrd Adlani on 27/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Domain

public typealias NetworkResponseCompletion = (Any?, Error?) -> ()

public protocol WebServiceProtocol {
    var dispatcher: NetworkDispatcherProtocol! { get set }
}

// MARK: WebService
public struct WebService {
    var dispatcher: NetworkDispatcherProtocol!
    
    public init(dispatcher: NetworkDispatcherProtocol = NetworkDispatcher()) {
        self.dispatcher = dispatcher
    }
}

public extension WebService {
    func getPolicies(completion: @escaping NetworkResponseCompletion) {
        GetPolicies().execute(dispatcher: dispatcher, onSuccess: { policies in
            completion(policies, nil)
        }) { error in
            completion(nil, error)
        }
    }
}

public struct GetPolicies: RequestTypeProtocol {
    public typealias ResponseType = Policies
    public var data: Request {
        return Request(path: EndPoints.policies.rawValue)
    }
}
