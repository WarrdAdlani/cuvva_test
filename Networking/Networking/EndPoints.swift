//
//  EndPoints.swift
//  Networking
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

enum EndPoints: String {
    case policies = "http://www.mocky.io/v2/5c9c99fe3600006c56d971ba"
    case standardTextApi = "http://www.mocky.io/v2/5c699176370000a90a07fd6f"
}
