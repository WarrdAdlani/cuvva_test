//
//  UIViewController+Extension.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 28/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

extension UIViewController {
    func showError(with error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let okay = UIAlertAction(title: "Okay", style: .default, handler: nil)
        alert.addAction(okay)
        present(alert, animated: false, completion: nil)
    }
}
