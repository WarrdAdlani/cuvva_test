//
//  UINavigationController+Extension.swift
//  Cuvva_Test
//
//  Created by Warrd Adlani on 03/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
}
